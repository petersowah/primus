<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Book A Bus</title>
<link rel="stylesheet" href="mystyles/book.css"/>
<link rel="shortcut icon" href="favicon.ico">
<script language="javascript" type="text/javascript" src="lang/datetimepicker_css.js"></script>
<style>
table{
	border: none;
	float: center;
}
td{
	border: none;
	text-align: center;
}
.origin,.departure,.destination{
	float: left;
}
.orig,.depart,.dest{
	float: right;
}
</style>
</head>
<body>

	<div id="big_wrapper">
		
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
				<?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?>
			</ul>
		</nav>
		
		<section id="main_section">
			<p><b>Welcome, <?php echo $_SESSION['username']; ?></b></p>
			<table>
				<tr>
					<td><img src="images\bus1.jpg" width="160" height="90" /></td>
					<td><img src="images\bus2.jpg" width="160" height="90" /></td>
					<td><img src="images\bus6.jpg" width="160" height="90" /></td>
					<td><img src="images\bus7.gif" width="160" height="90" /></td>
				</tr>
			</table>
			
			<div id="display_date">
			<?php echo date('D,d-M-Y'); ?>
		</div>			
					<?php
					if(isset($_POST['origin'])&&isset($_POST['destination'])&&isset($_POST['date'])){
					$_SESSION['origin']=mysql_real_escape_string($_POST['origin']);
					$_SESSION['destination']=mysql_real_escape_string($_POST['destination']);
					$_SESSION['date']=mysql_real_escape_string($_POST['date']);
					
					if(empty($_POST['origin'])&&empty($_POST['destination'])&&empty($_POST['date']))
					echo ('<h2 align="center">All fields are required.</h2>');
					}
					?>
					
						<div id="form_area">
							<form name="bus_check" method="post" action="available_buses.php">
							<table cellspacing="20">
								 <tr>
									 <td>
									<b class="orig">Origin:</b>
									 </td>
									<td class="origin">
										<select id ="origin" name="origin" >
											<option value="ORIG">-Select Origin-</option>
											<option value="ACC">Accra </option>
											<option value="KUM">Kumasi </option>
											<option value="SEK/TAK">Sekondi/Takoradi </option>
											<option value="TAM"> Tamale</option>
											<option value="HO"> Ho</option>
											<option value="CAPE">Cape Coast </option>
											<option value="SUNY"> Sunyani</option>
											<option value="WA">Wa </option>
											<option value="BOLGA">Bolgatanga</option>
											<option value="KOF">Koforidua </option>
											<option value="TEMA"> Tema</option>
										</select>   										<!--value is used when programming-->
									</td>
								</tr>
								
								<tr>
									<td>
									<b class="dest">Destination: </b>
									</td>
										
									<td class="destination"> 
										<select id="destination" name="destination" >
										<option value="DESTI">-Select Destination-</option>
									    <option value="ACC">Accra </option>
										<option value="KUM">Kumasi </option>
										<option value="SEK/TAK">Sekondi/Takoradi </option>
										<option value="TAM"> Tamale</option>
										<option value="HO"> Ho</option>
										<option value="CAPE">Cape Coast </option>
										<option value="SUNY"> Sunyani</option>
										<option value="WA">Wa </option>
										<option value="BOLGA">Bolgatanga</option>
										<option value="KOF">Koforidua </option>
										<option value="TEMA"> Tema</option>
										</select>
									</td>
								</tr>
								
								<tr>
									<td>
										<b class="depart">Departure Date: </b>
									</td>
									<td class="departure">
										<input name="date" id="demo3" type="text" size="25" placeholder="<?php echo date("Y-m-d");?>"><a href="javascript:NewCal('demo1','ddmmyyyy')">
										<img src="images/images2/cal.gif" onclick="javascript:NewCssCal ('demo3','yyyyMMdd','arrow')" style="cursor:pointer" width="16" height="16" border="0" alt="Pick a date"></a>	
											
									</td>
								</tr>
								<tr>
									<td>
									</td>
									<td>
										<input name="submit" type="submit" value="&nbsp Search Bus &nbsp" />
									</td>
								</tr>
							</table>
						</form>
						
					
			</div></br>
		</section>
		
		<footer id="the_footer">
			Copyright &copy 2013 Retep Innovations.
		</footer>

	</div>
	</body>
</html>
