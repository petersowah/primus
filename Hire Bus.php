<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" >
<title>Hire A Bus/Taxi</title>
<link rel="stylesheet" href="mystyles/main.css">
<link rel="shortcut icon" href="favicon.ico">
<script language="javascript" type="text/javascript" src="lang/datetimepicker_css.js"></script>
<style>
td{
	padding:5px;
	text-align:left;
}
.type,.booking_date{
	float:none;
}
#main_section{
	margin-left: 70px;
}
</style>
</head>

<body>
	<div id="big_wrapper">
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
				<?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?>
			</ul>
		</nav>
		
		<section id="main_section">
		
		<p>Primus have the most robust, luxurious taxi collection in Ghana at the moment. We have special buses which can be hired for tours as well as educational trips. Our taxis are mostly based at the Kotoka International Airport as well as our head office. The vehicles(buses and taxis) are well furnished, air-conditioned and very comfy to be driven in.</p><br/>
		
		<p>Our vehicles are driven by very well-trained drivers with experience in the industry.</p><br/>
		
		<p>As at now, our taxis ply the streets of Accra only and the price for hiring is fixed to anywhere in Accra. The buses are given out for a full day and charged accordingly. See below for our prices.</p><br/>
		
		<table border="1" cellpadding="5">
		<tr>
			<th>Vehicle Type</th>
			<th>Price(GH&#162;)</th>
		</tr>
		<tr>
			<td>Bus Ordinary</td>
			<td>GH&#162;220.00 per day</td>
		</tr>
		<tr>
			<td>Bus Luxury</td>
			<td>GH&#162;320.00 per day</td>
		</tr>
		<tr>
			<td>Taxi</td>
			<td>GH&#162;65.00 anywhere in Accra</td>
		</tr>
		</table><br/>
		<h3>Please fill the form below to reserve a bus/taxi.</h3>
		<?php/*
			if(isset($_POST['hire'])){
			$_SESSION['hire_type']=$_POST['hire_type'];
			$_SESSION['date']=$_POST['date'];
			$_SESSION['days']=$_POST['days'];
			}
		*/	?>
		
		<form method="POST" action="payment2.php">
		<table>
			<tr>
				<td>
					<b>Vehicle Type:</b>
				</td>
				<td class="type">
					<select id="hire_type" name="hire_type">
						<option value="">-Select-</option>
						<option value="bus_lux">Bus LUX</option>
						<option value="bus_ord">Bus ORD</option>
						<option value="taxi">Taxi</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					<b>No. of Days:</b>
				</td>
				<td class="days">
					<input name="days" id="days" type="text">
				</td>
			</tr>
			
			<tr>
				<td>
						<b>Booking Date: </b>
				</td>
				<td class="booking_date" name="booking_date">
					<input name="date" id="demo3" type="text" size="25" placeholder="<?php echo date("Y-m-d");?>"><a href="javascript:NewCal('demo1','ddmmyyyy')">
					<img src="images/images2/cal.gif" onclick="javascript:NewCssCal ('demo3','yyyyMMdd','arrow')" style="cursor:pointer" width="16" height="16" border="0" alt="Pick a date"></a>	
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<input name="Hire" type="submit" value="&nbsp Hire Now &nbsp" />
				</td>
			</tr>
		</table>
		
		</form>

		</section>
		<footer id="the_footer">
			Copyright &copy 2013 Primus Transport.<br/>
		<b>Powered by Retep Innovations GH.</b>
		</footer>

	</div>
</body>

</html>