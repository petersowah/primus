<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" >
<title>Ticket Cancellation</title>
<link rel="stylesheet" href="mystyles/main.css">
<link rel="shortcut icon" href="favicon.ico">
<style>
	.details{
		margin-left: 75px;
	}
	.caution{
		color:#FF0000;
	}
</style>
</head>


<body ONLOAD="document.getElementById('reservation_code').select(); ">
	
	<div id="big_wrapper">
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
				<?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?>
			</ul>
		</nav>
		
		<section id="main_section">
		<div class="details">
		<h1 align="center">Your current reservation details.</h1><br/><br/>
	
		<?php
			
			$_SESSION['code']=mysql_real_escape_string($_POST['reservation_code']);
			$code=$_SESSION['code'];
			
			$query="SELECT * FROM bookings WHERE reservation_code='$code'";
			$result = mysql_query($query);
			
			if(!$result){
			die("Database Query failed:" . mysql_error());
			}
			while($row = mysql_fetch_array($result)){ 
			echo("<b>Username:&nbsp;&nbsp;$row[1]</b><br/>");
			echo("<b>Bus No:&nbsp;&nbsp;$row[4]</b><br/>");
			echo("<b>Bus Type:&nbsp;&nbsp;$row[6]</b><br/>");
			echo("<b>Departure Date:&nbsp;&nbsp;$row[7]</b><br/>");
			echo("<b>Seats:&nbsp;&nbsp;$row[5] </b><br/>");
			echo("<b>Amount Paid:&nbsp;&nbsp;GH&#162;$row[12] </b><br/>");
			echo("<b>Phone:&nbsp;&nbsp;$row[10]</b><br/>");
			$_SESSION['bus_id']=$row[0];
			$bus_id=$_SESSION['bus_id'];
			}
			$bus_id=$_SESSION['bus_id'];
			$query = "SELECT * FROM available_buses WHERE id='$bus_id'";
			$result = mysql_query($query);
			
			if(!$result){
			die("Database Query failed:" . mysql_error());
			}
			while($row = mysql_fetch_array($result)){ 
			echo("<b>Duration of Journey:&nbsp;&nbsp;$row[7] Hrs.</b><br/>");
			echo("<b>Departure Time:&nbsp;&nbsp;$row[5]</b><br/><br/>");
			}
		?>
		
		
		
		<form method="POST" action="cancel_success.php">
		<input type="Submit" name="cancel" id="cancel" value="Cancel Reservation"/>
		<form>
		</div>
		</section>
		
		
		<footer id="the_footer">
			Copyright &copy 2013 Retep Innovations.
		</footer>

	</div>
</body>

</html>