<?php

require 'connect.inc.php';
?>

<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" >
<title>Bus Schedules & Routes</title>
<link rel="stylesheet" href="mystyles/schedule.css">
<link rel="shortcut icon" href="favicon.ico">
<style>
.table{
	margin-left:120px;
}
h3{font-size:21px;}
</style>
</head>
<body>
	
	<div id="big_wrapper">
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
			</ul>
		</nav>
		
		
		
		<section id="main_section">
		
				<h3 align="right"><u>Bus Schedules & Routes</u></h3><br/>
				
				 <div align =  "center">
	 
			<table width="750" border="1" cellpadding="5" class="table">
				<tr>
				<td><span style="font-weight:bold;">S/No.</span></td>
				<td><span style="font-weight:bold;">Bus Number</span></td>
				<td><span style="font-weight:bold;">Bus Type</span></td>
				<td><span style="font-weight:bold;">Origin</span></td>
				<td><span style="font-weight:bold;">Destination</span></td>
				<td><span style="font-weight:bold;">Departure Time</span></td>
				<td><span style="font-weight:bold;">ETA</span></td>
				<td><span style="font-weight:bold;">Fare(GH&#162;)</span></td>
				</tr>
			
			
		     <?php
						
			//start booking process
					$query = "SELECT * FROM available_buses";
					$result = mysql_query($query);
					
					if(!$result){
					die("Database Query failed:" . mysql_error());
					}
					while($row = mysql_fetch_array($result)){
					echo ("<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td><td>$row[7]</td><td>$row[8]</td></tr>");
					}
			?>
			
			</table>
			</div>
		</section>
		
		<footer id="the_footer">
			Copyright &copy 2013 Retep Innovations.
		</footer>

	</div>
</body>

</html>