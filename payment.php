<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Payment</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   <style>
			.logos{
				margin-top:5px;
				text-align:center;
				margin-left:250px;
				border:1px dashed black;
				margin-bottom:20px;
				}
			
				
			.details{
				margin-top:20px;
				text-align:right;
				margin-left:250px;
				border:1px solid black;
				margin-bottom:20px;
				float:center;
				}
			td{
				text-align:right;
			}	
			.accept{
				margin-left:180px;
			}
			#networks,#amount,#seat,#num,#PIN,#phone{
				float:left;
			}
			.pinreq{
			float:left;
			}
			h3{
			color:red;
			}
   </style>
</head>
<body ONLOAD="document.getElementById('phone').select(); ">
   <div id="big_wrapper">
      <header id="top_header">
         <img src="images/header/project.gif" alt="Bus for header" />
      </header>
      
      <nav id="top_menu">
         <ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
				<?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?>
         </ul>
      </nav>
      
         <div id="new_div">
      
         <section id="main_section">
			<p><b class="accept">We accept:</b></p><br/>
			<table class="logos">
				<tr>
					<td><img src="images\mobile_money\airtel.jpg" width="" height="" /></td>
					<td><img src="images\mobile_money\mtn.jpg" width="" height="" /></td>
					<td><img src="images\mobile_money\tigo.jpg" width="" height="" /></td>
				</tr>
			</table>

		<?php
			 if(isset($_POST['submit'])){
			 $charset='qwertyuiopasdfghjklmnbvcxz0123456789#$&)*(';
			 $_SESSION['reservation'] = mysql_real_escape_string(substr(str_shuffle($charset),0,8));
			 $seat=mysql_real_escape_string($_SESSION['seat']);
			 $seat_no=mysql_real_escape_string($_SESSION['seat_no']);
			 $username=mysql_real_escape_string($_SESSION['username']);
			 $origin=mysql_real_escape_string($_SESSION['origin']);
			 $destination=mysql_real_escape_string($_SESSION['destination']);
			 $bus_no=mysql_real_escape_string($_SESSION['bus_no']);
			 $bus_type=mysql_real_escape_string($_SESSION['bus_type']);
			 $seat=mysql_real_escape_string($_SESSION['seat']);
			 $date=mysql_real_escape_string($_SESSION['date']);
			 $busid=mysql_real_escape_string($_SESSION['busid']);
			 $phone=mysql_real_escape_string($_POST['phone']);
			 $pin=mysql_real_escape_string($_POST['pin']);
			 $reservation=mysql_real_escape_string($_SESSION['reservation']);
			 $dor=date("Y-m-d");
			 $amt=mysql_real_escape_string($_SESSION['fare'])*mysql_real_escape_string($_SESSION['seat']);
			 
			 
				if(!empty($pin)&&!empty($phone)){
				$query="SELECT * FROM bookings WHERE 'seat_no'='$seat'";
				$query_run = mysql_query($query);
				
				if(mysql_num_rows($query_run)==1){
				echo "Seat '.$seat_no.'already taken.";
				}else{
				$query="INSERT INTO bookings VALUES ('','".$username."','".$origin."','".$destination."','".$bus_no."','".$seat_no."','".$bus_type."','".$date."','".$reservation."','".$busid."','".$phone."','".$dor."','".$amt."')";
				if($query_run = mysql_query($query)){
				header('Location:reservation_code.php');
				}else{
				echo '<h3>Sorry, reservation not done. Please try again in a few minutes.</h3>';
				}
				}
				}else{
				echo '<h3>Please enter Phone number and PIN.</h3>';
				}
			}
		?>
			<form id="payment_details" method="POST" action="" >
			<table class="details">
					<tr>
						<td>
							<b>Network:</b>
						</td>
							
						<td> 
							<select id="networks" name="networks">
								  <option value="1">MTN Mobile Money</option>
								  <option value="2">tiGO Cash</option>
								  <option value="3">Airtel Money</option>
							</select>
						</td>
					</tr>
					
					 <tr>
						 <td>
							<b>Phone Number:</b>
						 </td>
						<td>  
							<input type="text" name="phone" id="phone" maxlength="10" size="10" />
						</td>
					</tr>
					
					<tr>
						<td>
							<b>PIN:</b>
						</td>
							
						<td> 
							<input type="password" name="pin" id="PIN" maxlength="4" size="4" />
						</td>
					</tr>
					
					<tr>
						<td>
							<b>Number of reserved seats: </b>
						</td>
						<td>	
							<?php $seat=mysql_real_escape_string($_SESSION['seat']);
							echo ("<b id='seat'>$seat</b>");?>	
						</td>
					</tr>
					
					<tr>
						<td>
							<b>Reserved seats: </b>
						</td>
						<td>	
							<?php $seat_num=mysql_real_escape_string($_SESSION['seat_no']);
							echo ("<b id='num'>$seat_num</b>");
							?>	
						</td>
					</tr>
					
					<tr>
							<td>
							<b>Amount to paid:</b>
							</td>
							<td id="amount">GH&#162;
								<?php $amt=mysql_real_escape_string($_SESSION['fare']) * mysql_real_escape_string($_SESSION['seat']);
								echo $amt;?>
							</td>
					</tr>
					
					<tr>
							<td colspan="1"><input name="submit" type="submit" /></td>
					</tr>
					
				</table>
			</form>
         </section>
 
         
         
         </div>
      
      <footer id="the_footer">
        Copyright &copy 2013 Primus Transport.<br/>
		<b>Powered by Retep Innovations.</b>
      </footer>
   </div>
</body>
</html>