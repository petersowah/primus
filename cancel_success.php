<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Cancel Reservation</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   
   <style>
	.details{
		margin-left: 75px;
	}
	.caution{
		color:#FF0000;
	}
	
</style>
</head>
<body>
   <div id="big_wrapper">
      <header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>
      
      <nav id="top_menu">
         <ul>
				<li><a href="index.php">Home</a></li>
				<li><?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:adminlogin.php');
				}?></li>
			</ul>
      </nav>
      
         <div id="new_div">
      
         <section id="main_section">
		<?php
			if(isset($_POST['cancel'])){
			
			$username=mysql_real_escape_string($_SESSION['username']);
			$code=mysql_real_escape_string($_SESSION['code']);
			
			 $query = "DELETE FROM bookings WHERE reservation_code='$code' and username='$username' LIMIT 1"; 
			 $query_run=mysql_query($query);
			 if(mysql_affected_rows()==1){
			 $message="<h1>Reservation was cancelled successfully.</h1><br/>";
			 echo $message;
			 }else{
			 $message="<h3 class='caution'>You do no have the authorisation to cancel this reservation.</h3>";
			  echo $message . mysql_error();
			 }
			 }
		?>
          </section>
          </div>
      
      <footer id="the_footer">
         Copyright &copy 2013 Retep Innovations.
      </footer>
   </div>
</body>
</html>