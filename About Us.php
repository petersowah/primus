<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" >
<title>About Us</title>
<link rel="stylesheet" href="mystyles/about.css">
<link rel="shortcut icon" href="favicon.ico">
</head>

<body>
	
	<div id="big_wrapper">
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
			</ul>
		</nav>
		
		<section id="main_section">
		<h1>About Primus</h1><br/>

<p>Primus.com is Ghana&#39;s largest and first online bus ticketing platform.</p><br/>

<p>It is revolutionizing the bus ticketing industry in Ghana by using technology to improve customer satisfaction and bus operator&#39;s profitability and redefining the entire supply chain. We ply routes such as Accra, Kumasi, Tema, Sunyani, Takoradi, Cape Coast, Tamale and others. Our buses are very comfortable and safe to travel in. </p> <br/>

<p>Primus&#39; e-bus platform has seen nearly 1.2 million bus ticket transactions all over Ghana till date and has become a dominantly strong player in the market today. </p><br/>

<p>We have seasoned drivers who have been trained thoroughly to carry our passengers safely to their destinations. Our rates are very moderate. Primus.com offers luxury Yutong, air-conditioned, non air-conditioned, seater, sleeper, seater cum sleeper and other types of buses across the length and breadth of the country.<p><br/>

<p>Primus.com also offers special bus charters, car/taxi bookings as well as domestic and international holiday packages. By leveraging the power of the internet and advanced technology, Primus.com provides real time prices and inventory position, multiple payment options (from credit cards to debit cards, internet banking, mobile money et all), easy cost/availability comparison, last minute bookings, customer support and even home delivery of tickets.</p><br/>

<p>Primus.com understands the intensity of the travel industry and has integrated it with implausible technology that has enabled it to create the best end-to-end customer experience. This is backed by a soaring-high range of best-value products and services and dedicated customer support.<p><br/>

<p>Remaining reliable, efficient and at the forefront of technology, Primus&#39; commitment and customer-centricity allows it to better understand and provide for its customers&#39; diverse needs and wants, and deliver consistently. Also, Primus.com is deeply committed to its vision of providing a distinctive travel platform to bring value for money&#39; to all members of the ecosystem with its simple, easy to book and cost effective services.</p><br/>

</section>
		<footer id="the_footer">
			Copyright &copy 2013 Retep Innovations.
		</footer>

	</div>
</body>

</html>