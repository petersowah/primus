<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" >
<title>Ticket Cancellation</title>
<link rel="stylesheet" href="mystyles/main.css">
<link rel="shortcut icon" href="favicon.ico">
<style>
	.details{
		margin-left: 75px;
	}
	.caution{
		color:#FF0000;
	}
</style>
</head>


<body ONLOAD="document.getElementById('reservation_code').select(); ">
	
	<div id="big_wrapper">
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
				<?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?>
			</ul>
		</nav>
		
		<section id="main_section">
		<div class="details">
		<h3 align="center"> View/Cancel Bus Reservation.</h3>
		<!--
		<?php/*
		if(empty($_POST['submit'])){
				$message= '<h3 class="caution" align="center">Reservation Code required.</h3>';
				echo $message;
			}*/
		?>
		-->
		<form method = "post" action="reserve_alter.php">
		<b >Please enter reservation code:</b>	<input type="text" name="reservation_code" id="reservation_code" /><br/>
		<input type="Submit" name="submit" id="submit" value="&nbsp;Submit&nbsp;"/>
		</form>
		</div>
		</section>
		
		<footer id="the_footer">
			Copyright &copy 2013 Primus Transport.<br/>
		<b>Powered by Retep Innovations.</b>
		</footer>

	</div>
</body>

</html>