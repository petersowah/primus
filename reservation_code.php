<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Reservation Code</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   <style>
	p{
		color:#FF0000;
	}
</style>
</head>
<body>
   <div id="big_wrapper">
     <header id="top_header">
	<img src="images/header/project.gif" alt="Bus for header" />
</header>
      
      <nav id="top_menu">
         <ul>
            <li><a href="index.php">Home</a></li>
			<li><?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?></li>
         </ul>
      </nav>
      
        <div id="new_div">
			<section id="main_section">
					<?php
				$reservation = $_SESSION['reservation'];
				echo "<h1>Your reservation code is: $reservation</h1>";
				echo "<p>*Please keep this code at all times.</p><br/><br/>";
				
				echo "<h1>Thank you for using our services.</h1><br/>";
				?>    
			</section>
         
        </div>
      
	  <footer id="the_footer">
         Copyright &copy 2013 Retep Innovations.
      </footer>
   </div>
</body>
</html>