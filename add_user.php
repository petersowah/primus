<?php
require 'connect.inc.php';
require 'core.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Admin-Add User</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   
<style>
	#main_section{
	margin: 30 px;   /* This is 720 px, 280 left*/
}
	#reg_form{
	float: center;
	text-align: center;
	margin-left:200px;
}
</style>
</head>
<body ONLOAD="document.getElementById('username').select(); ">
<div id="big_wrapper">
<header id="top_header">
	<img src="images/header/project.gif" alt="Bus for header" />
</header>

<nav id="top_menu">
<ul>
<li><a href="admin.php">Admin Home</a></li>
<li><?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:adminlogin.php');
				}?></li>
</nav>

<?php
if(isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['password2'])&&isset($_POST['fname'])&&isset($_POST['sname'])&&isset($_POST['phone'])){
			$username=trim(mysql_real_escape_string(htmlentities($_POST['username'])));
			$password=trim(mysql_real_escape_string(htmlentities($_POST['password'])));
			$password2=trim(mysql_real_escape_string(htmlentities($_POST['password2'])));
			$password_hash=md5($password);
			$fname =mysql_real_escape_string($_POST['fname']);
			$sname =mysql_real_escape_string($_POST['sname']);
			$phone	=mysql_real_escape_string($_POST['phone']);

			if(!empty($username)&&!empty($password)&&!empty($password2)&&!empty($fname)&&!empty($sname)&&!empty($phone)){		
				if($password!=$password2){
				echo('<h2 align="center">Passwords do not match! Please re-enter password.</h2>');
				}else{
				//start registration process
		$query = "SELECT username FROM customers WHERE username = '$username'";		//checking if username already exists.
		$query_run = mysql_query($query);
		
		if (mysql_num_rows($query_run)==1) {
		echo ('<h2 align="center">The username '.$username.' already exists.</h2>');
		}else{
		$query = "INSERT INTO customers VALUES ('','".mysql_real_escape_string($username)."','".mysql_real_escape_string($password_hash)."','".mysql_real_escape_string($fname)."','".mysql_real_escape_string($sname)."','".mysql_real_escape_string($phone)."')";
		
		if ($query_run = mysql_query($query)){
			header('Location:registration_success.html');
		}else{
		echo('<h2>Sorry! We couldn\'t register you at this moment. Please try again later.</h2>');
		}
		}
		}
	}else{
				echo '<h3 align="center">All fields are required!</h3>';
					}
			}
?>

	<!-- Registration Form Begin-->

<form method="POST" action="add_user.php">    
	<table id="reg_form" cellspacing="20" >
			<tr>
				<td><b>Username: </b></td>
				<td><input id="username" type="text" name="username" maxlength="30" value="<?php  if(isset($_POST['username'])) echo htmlentities($username); ?>" /></td>
			</tr>
			<tr>
				<td><b>Password: </b></td>
				<td><input type="password" name="password" maxlength="25" /></td>
			</tr>
			<tr>
				<td><b>Confirm Password: </b></td>
				<td><input type="password" name="password2" maxlength="25" /></td>
			</tr>
			
			<tr>
				<td><b>First Name: </b></td>
				<td><input type="text" name="fname" maxlength="30" value="<?php  if(isset($_POST['fname'])) echo htmlentities($fname); ?>"/></td>
			</tr>
			
			<tr>
				<td><b>Surname: </b></td>
				<td><input type="text" name="sname" maxlength="30" value="<?php  if(isset($_POST['sname'])) echo htmlentities($sname); ?>" /></td>
			</tr>
			<tr>
				<td><b>Phone Number: </b></td>
				<td><input type="text" name="phone" maxlength="10" value="<?php  if(isset($_POST['phone'])) echo htmlentities($phone); ?>" /></td>
			</tr>
			
			<th>
				<td><p><input type="submit" name="submit" value=" Add User " align="center" colspan="2"  /></td>
			</th>
	</table>
</form>    <!-- Registration Form End-->
			
</div>
</body>
</html>