
<!Doctype html>
<html lang="en">
<head>
<meta charset="utf-8" >
<title>Contact Us</title>
<link rel="stylesheet" href="mystyles/contact.css">
<link rel="shortcut icon" href="favicon.ico">
<style>
		#contact{
				border-radius:10px;
		}
   </style>
</head>


<body>
	
	<div id="big_wrapper">
		<header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>

		<nav id="top_menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
			</ul>
		</nav>
		
		<section id="main_section">
		
			<h1>&nbsp Contact us: </h1><br/>
			
			<?php
				if (isset($_POST['contact_name'])&&isset($_POST['contact_email'])&&isset($_POST['contact_text'])){
				 $contact_name=$_POST['contact_name'];
				 $contact_email=$_POST['contact_email'];
				 $contact_text = $_POST['contact_text'];
				 
				 if (!empty($contact_name) && !empty($contact_email) && !empty($contact_text)){
				 
				    $to='petersowah@gmail.com';
					$subject='Contact form submitted.';
					$body=$contact_name."\n".$contact_text;
					$headers = 'From: '.$contact_email;
					
					if(@mail($to,$subject,$body,$headers)){
				     echo '<h1>Thanks for contacting us.We\'ll get back to you soon.</h1><br/>';
				}else{
						echo '<h1>Sorry, an error occured. Please try again later.</h1><br/>';
					}
				}else{
					echo '<h1>All fields are required!</h1><br/>';
				 }
				}
		?>
			<form action="contact us.php" method="post">
				<b>&nbsp Name: </b> <input type="text" size="45" name="contact_name" /> <br/><br/>
				<b>&nbsp Email: </b> <input type="text" name="contact_email" size="45" /> <br/><br/>
				<b>&nbsp Message: <br/>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</b> <textarea name="contact_text" rows="8" cols="45" placeholder="Type Message Here..."></textarea><br/><br/>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
				<input type="submit" value="Send" />
			</form>
			</section>
			
			<div id="contact">
			<p><u><h1>Call Us on: </h1></u></p><br/>
			<p><h2>Mob: +233-24-375-3852 </h2></p><br/>
			<p><h2>Tel: +233-30-271-5322 </h2></p><br/>
			<p><h2>Email: <a href="#" >info@primus-services.com.gh </a></h2></p><br/>
			
			<p><h2>Locate Us at The Neoplan Station, Circle-Accra.</h2></p><br/>
			</div>
			
		<footer id="the_footer">
			Copyright &copy 2013 Retep Innovations.
		</footer>

	</div>
</body>

</html>