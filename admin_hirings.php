<?php
require 'core.inc.php';
require 'connect.inc.php';
?>
<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Admin-View Hiring</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   
   <style>
		table {
				margin-top:20px;
				border-color:#E5E5E5;
				text-align:center;
				margin-left:20px;
				border:1px solid black;
				margin-bottom:20px;
				}
			td{
				padding:2px;
				background-color:#E5E5E5;
				border:0px;
				}

   </style>
</head>
<body>
   <div id="big_wrapper">
      <header id="top_header">
         <img src="images/header/project.gif" alt="Bus for header" />
      </header>
      
      <nav id="top_menu">
         <ul>
				<li><a href="admin.php">Admin Home</a></li>
				<li><?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?></li>
         </ul>
      </nav>
	  <div align =  "center">
		<div align="center"><br/><h1>Bookings</h1></div>
	  <table border = "2" cellspacing = "5" >
		<tr>
			<td><b>No.</b></td>
			<td><b>Username</b></td>
			<td><b>Vehicle Type</b></td>
			<td><b>Booking Date</b></td>
			<td><b>Duration</b></td>
			<td><b>Reservation Code</b></td>	
			<td><b>Departure Date</b></td>
			<td><b>Phone</b></td>
			<td><b>Amount(GH&#162;)</b></td>
		</tr>
			
	<?php
			//start booking process
			$query = "SELECT * FROM hiring";
			$result = mysql_query($query);
			
			if(!$result){
			die("Database Query failed:" . mysql_error());
			}
			while($row = mysql_fetch_array($result)){ 
			echo ("<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td><td>$row[6]</td><td>$row[7]</td><td>$row[8]</td></tr>");
			}
	?>
			</table>
			
			</div>
      
      <footer id="the_footer">
         Copyright &copy 2013 Retep Innovations.
      </footer>

</body>
</html>