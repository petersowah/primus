<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Online Bus Reservation</title>
   <link href="mystyles/main.css" rel="stylesheet" type="text/css" />
   <link href="mystyles/js-image-slider.css" rel="stylesheet" type="text/css" />
   <link rel="shortcut icon" href="favicon.ico">
   <script src="lang/js-image-slider.js" type="text/javascript"></script>
   <style>
      #side_news{
	       border-radius:10px;
		}
   </style>
   
</head>
<body>
		<div id="big_wrapper">

		   <header id="top_header">
				<img src="images/header/project.gif" alt="Bus for header" />
			</header>
		   
		   <nav id="top_menu">
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="book.php">Book Bus</a></li>
					<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
					<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
					<li><a href="schedules.php">Schedules & Our Routes</a></li>
					<li><a href="Contact Us.php">Contact Us</a></li>
					<li><a href="About Us.php">About us</a></li>
				</ul>	
		   </nav>
		   
			<section id="main_section"> 
				<article>
					<marquee><h2>Ghana's first online bus reservation website.<h2></marquee>
				</article>
					 <div id="slider">
						<img src="images/bus8.jpg" alt="Interior of Bus" />
						<img src="images/bus11.jpg" />
						<a href="Hire Bus.php"><img src="images/taxi2.jpg" alt="Click on image to hire a taxi now!"/></a>
						<img src="images/bus4.jpg" />
						<img src="images/taxi1.jpg" alt="Hire A Taxi" />
					</div>
			</section>
		   
		   <aside id="side_news">
			  <h4 align="center">News</h4>
			  <h5 align="center">Buses and Coaches</h5>
			  
			  <!--News on Transport in Ghana-->
<p>Government has recently introduced large buses on some routes, for mass transportation in the cities and intra cities. This is not yet entirely efficient but has helped ease some of the congestion in town. A few London styled double-decker buses have also been introduced on certain routes. These are the cheapest forms of Transport Union (GPRTU) runs a fairly effective service. Read more...<br/>
<b>SOURCE</b>-<a href="http://www.ghananation.com/tourism/Buses-and_Coaches.asp" target="_blank">Ghana Nation</a>
		   </aside>
		 
		 <!--social network links-->
		 
		   <div class="social_net" >
				<a href="#"><img src="images/twitter.png"/></a>
				<a href="#"><img src="images/facebook.png"/></a>
			</div>
		
		   <footer id="the_footer">
			  Copyright &copy 2013 Primus Transport.
			  Powered by Retep Innovations.
		   </footer>
	   </div>
</body>
</html>