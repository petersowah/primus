<?php
require 'core.inc.php';
require 'connect.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Payments</title>
   <link rel="stylesheet" href="mystyles/book.css"/>
   <link rel="shortcut icon" href="favicon.ico">
     <style>
			.logos{
				margin-top:5px;
				text-align:center;
				margin-left:250px;
				border:1px dashed black;
				margin-bottom:20px;
				}
			.details{
				margin-top:20px;
				text-align:right;
				margin-left:250px;
				border:1px solid black;
				margin-bottom:20px;
				float:center;
				}
			td{
				text-align:right;
			}	
			.accept{
				margin-left:180px;
			}
			#networks,#amount,#seat,#num,#PIN,#phone{
				float:left;
			}
			.pinreq{
			float:left;
			}
			h4{
				color:red;
			}
   </style>
</head>
<body>
<div id="big_wrapper">
	  <header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>
	   
	   <nav id="top_menu">
		  <ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="book.php">Book Bus</a></li>
				<li><a href="Hire Bus.php">Hire Bus/Taxi</a></li>
				<li><a href="Ticket Cancellation.php">Ticket Cancellation</a></li>
				<li><a href="schedules.php">Schedules & Our Routes</a></li>
				<li><a href="Contact Us.php">Contact Us</a></li>
				<li><a href="About Us.php">About us</a></li>
				<?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:login.php');
				}?>
         </ul>
	   </nav>
	   
	   <section id="main_section"> 
		  <p><b class="accept">We accept:</b></p><br/>
			<table class="logos">
				<tr>
					<td><img src="images\mobile_money\airtel.jpg" width="" height="" /></td>
					<td><img src="images\mobile_money\mtn.jpg" width="" height="" /></td>
					<td><img src="images\mobile_money\tigo.jpg" width="" height="" /></td>
				</tr>
			</table>
			<?php
			$_SESSION['hire_type']=$_POST['hire_type'];
			$_SESSION['days']=$_POST['days'];
			$type=$_SESSION['hire_type'];
			$days=$_SESSION['days'];
			
			if($type=='bus_lux'){
			$price=320.00;
			}elseif($type=='bus_ord'){
			$price=220.00;
			}else{
			$price=65.00;
			}
			?>
			<?php
			 $date=mysql_real_escape_string($_POST['date']);
             if(isset($_POST['submit'])){
			 $charset='qwertyuiopasdfghjklmnbvcxz0123456789#$&)*(';
			 $_SESSION['reservation2'] = mysql_real_escape_string(substr(str_shuffle($charset),0,8));
			 $username=mysql_real_escape_string($_SESSION['username']);
			 $phone=mysql_real_escape_string($_POST['phone']);
			 $pin=mysql_real_escape_string($_POST['pin']);
			 $date=mysql_real_escape_string($_SESSION['date']);
			 $reservation=mysql_real_escape_string($_SESSION['reservation2']);
			 $dor=date("Y-m-d");
			 $amt=$price*$_SESSION['days'];
			 $_SESSION['days']=$_POST['days'];
			 $days=mysql_real_escape_string($_SESSION['days']);
			 $_SESSION['hire_type']=$_POST['hire_type'];
			 $type=mysql_real_escape_string($_SESSION['hire_type']);
			 
				if(!empty($pin)&&!empty($phone)){
				$query="INSERT INTO hiring VALUES ('','".$username."','".$type."','".$date."','".$days."','".$reservation."','".$dor."','".$phone."','".$amt."')";
				if($query_run = mysql_query($query)){
				header('Location:reservation_code2.php');
				}else{
				echo 'Sorry, reservation not done. Please try again in a few minutes.';
				}
				}else{
				echo 'Please enter Phone number and PIN.';
				}
			}
			?> 
			<form id="payment_details" method="POST" action="" >
			<table class="details">
					<tr>
						<td>
							<b>Network:</b>
						</td>
							
						<td> 
							<select id="networks" name="networks">
								  <option value="1">MTN Mobile Money</option>
								  <option value="2">tiGO Cash</option>
								  <option value="3">Airtel Money</option>
							</select>
						</td>
					</tr>
					
					 <tr>
						 <td>
							<b>Phone Number:</b>
						 </td>
						<td>  
							<input type="text" name="phone" id="phone" maxlength="10" size="10" />
						</td>
					</tr>
					
					<tr>
						<td>
							<b>PIN:</b>
						</td>
							
						<td> 
							<input type="password" name="pin" id="PIN" maxlength="4" size="4" />
						</td>
					</tr>
				
					<tr>
						<td>
						<b>Amount to paid:</b>
						</td>
						<td id="amount" name="amount">GH&#162;
							<?php $amt=$price*$_SESSION['days'];
							echo $amt;
							?>
						</td>
					</tr>
					<tr>
							<td colspan="1"><input name="submit" type="submit" /></td>
					</tr>
				</table>
			</form>
	   </section>
	   <footer id="the_footer">
		Copyright &copy 2013 Primus Transport.<br/>
		<b>Powered by Retep Innovations.</b>
	   </footer>
   </div>
</body>
</html>