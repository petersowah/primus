<?php
require 'connect.inc.php';
require 'core.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Admin-Add Bus details</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   <script language="javascript" type="text/javascript" src="lang/datetimepicker_css.js"></script>
<style>
	#main_section{
	margin: 30 px;   /* This is 720 px, 280 left*/
}
	#reg_form{
	float: center;
	text-align: center;
	margin-left:200px;
}
h3{
	color:red;
}
</style>
</head>
<body ONLOAD="document.getElementById('username').select(); ">
<div id="big_wrapper">
<header id="top_header">
	<img src="images/header/project.gif" alt="Bus for header" />
</header>

<nav id="top_menu">
<ul>
<li><a href="admin.php">Admin Home</a></li>
<li><?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:adminlogin.php');
				}?></li>
				
</nav>
<p><h1 align="center">Please add Bus details below in BLOCK.</h1></p> <br/>
<?php
	if(isset($_POST['add_bus'])){
	$bus_no=mysql_real_escape_string($_POST['bus_no']);
	$bus_type=mysql_real_escape_string($_POST['bus_type']);
	$origin=mysql_real_escape_string($_POST['origin']);
	$destination=mysql_real_escape_string($_POST['destination']);
	$departure=mysql_real_escape_string($_POST['departure']);
	$date=mysql_real_escape_string($_POST['date']);
	$eta=mysql_real_escape_string($_POST['eta']);
	$fare=mysql_real_escape_string($_POST['fare']);
	
	if(!empty($bus_no)&&!empty($bus_type)&&!empty($origin)&&!empty($destination)&&!empty($departure)&&!empty($eta)&&!empty($fare)){
	$query = "SELECT bus_no FROM available_buses WHERE bus_no = '$bus_no'";		//checking if bus already exists.
	$query_run = mysql_query($query);
	
	if (mysql_num_rows($query_run)==1) {
		echo ('<h3 align="center">The bus '.$bus_no.' already exists.</h3>');
		}else{
		$query = "INSERT INTO available_buses VALUES ('','".mysql_real_escape_string($bus_no)."','".mysql_real_escape_string($bus_type)."','".mysql_real_escape_string($origin)."','".mysql_real_escape_string($destination)."','".mysql_real_escape_string($departure)."','".mysql_real_escape_string($date)."','".mysql_real_escape_string($eta)."','".mysql_real_escape_string($fare)."')";
		
		if ($query_run = mysql_query($query)){
			header('Location:addbus_success.html');
		}else{
		echo('<h3 align="center">Sorry! Bus was not added. Please try again later.</h3>');
		}
		}
	 
	 }else{
			echo '<h3 align="center">All fields are required!</h3>';
				}
		}
?>

<form method="POST" action="">   

	<table id="reg_form" cellspacing="20" >
			<tr>
				<td><b>Bus Registration No:</b></td>
				<td><input id="bus_no" type="text" name="bus_no" maxlength="15" value="<?php  if(isset($_POST['bus_no'])) echo htmlentities($bus_no); ?>" /></td>
			</tr>
			
			<tr>
				<td><b>Bus Type:</b></td>
				<td><input id="bus_type" type="text" name="bus_type" maxlength="15" value="<?php  if(isset($_POST['bus_type'])) echo htmlentities($bus_type); ?>" /></td>
			</tr>
			
			<tr>
				<td><b>Bus Origin:</b></td>
				<td><input id="origin" type="text" name="origin" maxlength="15" value="<?php  if(isset($_POST['origin'])) echo htmlentities($origin); ?>" /></td>
			</tr>
			
			<tr>
				<td><b>Bus Destination:</b></td>
				<td><input id="destination" type="text" name="destination" maxlength="15" value="<?php  if(isset($_POST['destination'])) echo htmlentities($destination); ?>" /></td>
			</tr>
			
			<tr>
				<td><b>Departure Time:</b></td>
				<td><input id="departure" type="text" name="departure" maxlength="15" value="<?php  if(isset($_POST['departure'])) echo htmlentities($departure); ?>" /></td>
			</tr>
			
			<tr>
				<td>
						<b>Departure Date: </b>
				</td>
				<td class="departure_date" name="">
					<input name="date" id="demo3" type="text" size="15" placeholder="<?php echo date("Y-m-d");?>"><a href="javascript:NewCal('demo1','ddmmyyyy')">
					<img src="images/images2/cal.gif" onclick="javascript:NewCssCal ('demo3','yyyyMMdd','arrow')" style="cursor:pointer" width="16" height="16" border="0" alt="Pick a date"></a>	
				</td>
			</tr>
			
			<tr>
				<td><b>ETA:</b></td>
				<td><input id="eta" type="text" name="eta" maxlength="15" value="<?php  if(isset($_POST['eta'])) echo htmlentities($eta); ?>" /></td>
			</tr>
			
			<tr>
				<td><b>Bus Fare(GH&#162;):</b></td>
				<td><input id="fare" type="text" name="fare" maxlength="15" value="<?php  if(isset($_POST['fare'])) echo htmlentities($fare); ?>" /></td>
			</tr>
			
			<th>
				<td><p><input type="submit" name="add_bus" value=" Add Bus " align="center" colspan="2"  /></td>
			</th>
	</table>
</form>    <!-- Registration Form End-->
			
</div>
</body>
</html>