<?php
require 'connect.inc.php';
require 'core.inc.php';
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Admin Page</title>
   <link rel="stylesheet" href="mystyles/main.css" />
   <link rel="shortcut icon" href="favicon.ico">
   <style>
  #main_section{
	margin-left:250px;
  }
   #main_section ul{
   list-style-image:none;
   }
  #main_section ul #add_admin{
	list-style-image:url(images/admin/addadmin.jpg);
  }
   #main_section ul #add_user{
	list-style-image:url(images/admin/adduser.jpg);
  }
   #main_section ul #del_user{
	list-style-image:url(images/admin/del_user.jpg);
  }
  #main_section ul #bookings{
	list-style-image:url(images/admin/bookings.png);
  }
  #main_section ul #hirings{
	list-style-image:url(images/admin/hiring.png);
  }
  #main_section ul #ticket{
	list-style-image:url(images/admin/cancel.jpg);
  }
  #main_section ul #bus{
	list-style-image:url(images/admin/add_bus.jpg);
  }
  
   </style>
</head>
<body>
   <div id="big_wrapper">
      <header id="top_header">
			<img src="images/header/project.gif" alt="Bus for header" />
		</header>
      
      <nav id="top_menu">
         <ul>
            <li>Go to <a href="index.php">Public Page</a></li>
			<li><?php if(loggedin()){
					echo "<a href='logout.php'>Log Out</a>";
				}else{
					 header('Location:adminlogin.php');
				}?></li>
		</ul>
      </nav>
		<section id="main_section">
		<h1>Welcome to Administrator's page.</h1><br/>
        <ul>
			<li id="add_admin"><a href="add_admin.php">Add Admin</a></li><br/>
			<li id="add_user"><a href="add_user.php">Add user</a></li><br/>
			<li id="del_user"><a href="del_user.php">Delete User</a></li><br/>
			<li id="bus"><a href="addbus.php">Add Bus,Destination & Origin</a></li><br/>
			<li id="ticket"><a href="admin_ticket_cancel.php">Cancel Ticket</a></li><br/>
			<li id="bookings"><a href="admin_bookings.php">View bookings</a></li><br/>
			<li id="hirings"><a href="admin_hirings.php">View hirings</a></li><br/>
		</ul>    
        </section>
         <footer id="the_footer">
         Copyright &copy 2013 Primus Tranport.<br/>
		 Powered by Retep Innovations GH.
		 </footer>
   </div>
</body>
</html>